/*
 * Variables.cpp
 *
 *  Created on: 10 Aug. 2017
 *      Author: tashy
 */

#include <iostream>
using namespace std;

int main()
{

//	float inputCash = 0.0f;
//	int cash = 0;
//	int hundred_dollar = 0;
//	int fifty_dollar = 0;
//	int twenty_dollar = 0;
//	int ten_dollar = 0;
//	int five_dollar = 0;
//	int two_dollar = 0;
//	int one_dollar = 0;
//	int fifty_cent = 0;
//	int twenty_cent = 0;
//	int ten_cent = 0;
//	int five_cent = 0;
//
//	cout << "Enter amount of cash in dollars and cents: ";
//	cin >> inputCash;
//	inputCash *= 100;
//	cash =  inputCash;
//
//	hundred_dollar = cash / 10000;
//	cash -= (hundred_dollar * 10000);
//	fifty_dollar = cash / 5000;
//	cash -= (fifty_dollar * 5000);
//	twenty_dollar = cash / 2000;
//	cash -= (twenty_dollar * 2000);
//	ten_dollar = cash / 1000;
//	cash -= (ten_dollar * 1000);
//	five_dollar = cash / 500;
//	cash -= (five_dollar * 500);
//	two_dollar = cash / 200;
//	cash -= (two_dollar * 200);
//	one_dollar = cash / 100;
//	cash -= (one_dollar * 100);
//	fifty_cent = cash / 50;
//	cash -= (fifty_cent * 50);
//	twenty_cent = cash / 20;
//	cash -= (twenty_cent * 20);
//	ten_cent = cash / 10;
//	cash -= (ten_cent * 10);
//	five_cent = cash / 5;
//	cash -= (five_cent * 5);
//
//
//	cout << "Breakdown in tender:" << endl;
//	cout << hundred_dollar << " hundred dollar bills" << endl;
//	cout << fifty_dollar << " fifty dollar bills" << endl;
//	cout << twenty_dollar << " twenty dollar bills" << endl;
//	cout << ten_dollar << " ten dollar bills" << endl;
//	cout << five_dollar << " five dollar bills" << endl;
//	cout << two_dollar << " two dollar coins" << endl;
//	cout << one_dollar << " one dollar coins" << endl;
//	cout << fifty_cent << " fifty cent coins" << endl;
//	cout << twenty_cent << " twenty cent coins" << endl;
//	cout << ten_cent << " ten cent coins" << endl;
//	cout << five_cent << " five cent coins" << endl;
//	cout << cash << " left over" << endl;

	int seconds = 0;
	int hours = 0;
	int minutes = 0;

	cout << "Enter number of seconds: ";
	cin >> seconds;

	hours = seconds / 3600;
	seconds -= (hours * 3600);
	minutes = seconds / 60;
	seconds -= (minutes * 60);

	cout << hours << "::" << minutes << "::" << seconds << endl;


	return 0;
}


