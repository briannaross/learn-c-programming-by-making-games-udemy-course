/*
 * Looping.cpp
 *
 *  Created on: 14 Aug. 2017
 *      Author: tashy
 */




#include <iostream>
#include <math.h>
using namespace std;



int main()
{
		int diamondHalfHeight = 0;

		cout << "Enter a number: ";
		cin >> diamondHalfHeight;

		for (int x = 0; x < diamondHalfHeight; x++) {
			for (int i = 1; i <= diamondHalfHeight - x; i++)
				cout << '*';
			for (int j = 1; j <= (x * 2); j++)
				cout << ' ';
			for (int k = 1; k <= diamondHalfHeight - x; k++)
				cout << '*';
			cout << endl;
		}


		for (int x = diamondHalfHeight - 1; x >= 0; x--) {
			for (int i = 1; i <= diamondHalfHeight - x; i++)
				cout << '*';
			for (int j = 1; j <= (x * 2); j++)
				cout << ' ';
			for (int k = 1; k <= diamondHalfHeight - x; k++)
				cout << '*';
			cout << endl;
		}

	return 0;
}


//void Problem5() {
//	int pyramid_height = 0;
//	cout << "Enter a number: ";
//	cin >> pyramid_height;
//	for (int x = 1; x <= pyramid_height; x++) {
//		for (int i = 1; i <= pyramid_height - x; i++)
//			cout << ' ';
//		for (int j = 1; j <= (x * 2) - 1; j++)
//			cout << '*';
//		for (int k = 1; k <= pyramid_height - x; k++)
//			cout << ' ';
//		cout << endl;
//	}
//	for (int x = pyramid_height - 1; x >= 0; x--) {
//		for (int i = 1; i <= pyramid_height - x; i++)
//			cout << ' ';
//		for (int j = 1; j <= (x * 2) - 1; j++)
//			cout << '*';
//		for (int k = 1; k <= pyramid_height - x; k++)
//			cout << ' ';
//		cout << endl;
//	}
//}


//void Problem4() {
//	int num = 0;
//	int numLen = 0;
//	int tempNum = 0;
//	bool isPalindrome = true;
//	cout << "Enter a palindrome: ";
//	cin >> num;
//	tempNum = num;
//	while (tempNum > 0) {
//		numLen++;
//		tempNum /= 10;
//	}
//	while (num > 9) {
//		int modulo = (num % 10);
//		int othernum = (num / (pow(10, numLen - 1)));
//		if (modulo != othernum) {
//			isPalindrome = false;
//		}
//		num -= (pow(10, numLen - 1) * modulo);
//		num /= 10;
//		numLen -= 2;
//	}
//	if (isPalindrome)
//		cout << "Number is a palindrome" << endl;
//	else
//		cout << "Number is a not palindrome" << endl;
//}


//void Problem3() {
//	int zeroes = 0;
//	int ones = 0;
//	int twos = 0;
//	int threes = 0;
//	int fours = 0;
//	int fives = 0;
//	int sixes = 0;
//	int sevens = 0;
//	int eights = 0;
//	int nines = 0;
//	int inputNum = 0;
//	cout << "Input a number: ";
//	cin >> inputNum;
//	while (inputNum != 0) {
//		switch (inputNum % 10) {
//		case 0:
//			zeroes++;
//			break;
//		case 1:
//			ones++;
//			break;
//		case 2:
//			twos++;
//			break;
//		case 3:
//			threes++;
//			break;
//		case 4:
//			fours++;
//			break;
//		case 5:
//			fives++;
//			break;
//		case 6:
//			sixes++;
//			break;
//		case 7:
//			sevens++;
//			break;
//		case 8:
//			eights++;
//			break;
//		case 9:
//			nines++;
//			break;
//		default:
//			break;
//		}
//		inputNum /= 10;
//	}
//	cout << "Frequency of 0 = " << zeroes << endl;
//	cout << "Frequency of 1 = " << ones << endl;
//	cout << "Frequency of 2 = " << twos << endl;
//	cout << "Frequency of 3 = " << threes << endl;
//	cout << "Frequency of 4 = " << fours << endl;
//	cout << "Frequency of 5 = " << fives << endl;
//	cout << "Frequency of 6 = " << sixes << endl;
//	cout << "Frequency of 7 = " << sevens << endl;
//	cout << "Frequency of 8 = " << eights << endl;
//	cout << "Frequency of 9 = " << nines << endl;
//}


//void Problem2() {
//	int asciiNum = 0;
//	bool isInput = true;
//	do {
//		cout << "Enter a number: ";
//		cin >> asciiNum;
//		if ((asciiNum >= 0) && (asciiNum <= 255))
//			cout << "ASCII character: " << (char) (asciiNum) << endl;
//		else if ((asciiNum < -1) || (asciiNum > 255))
//			cout << "Invalid number" << endl;
//		else if (asciiNum == -1) {
//			isInput = false;
//		}
//	} while (isInput == true);
//}


//void Problem1() {
//	char binDigit = 0;
//	bool isBinary = true;
//	cout << "Enter a binary number: ";
//	do {
//		cin >> binDigit;
//		if (binDigit == '0')
//			cout << '1';
//		else if (binDigit == '1')
//			cout << '0';
//		else if (binDigit == '\n')
//			isBinary = false;
//		else {
//			cout << "<Invalid>";
//			isBinary = false;
//		}
//	} while (isBinary);
//}


//void Exercise3() {
//	int pyramid_height = 0;
//	cout << "Enter a number: ";
//	cin >> pyramid_height;
//	for (int x = 1; x <= pyramid_height; x++) {
//		for (int i = 1; i <= pyramid_height - x; i++)
//			cout << ' ';
//		for (int j = 1; j <= (x * 2) - 1; j++)
//			cout << '*';
//		for (int k = 1; k <= pyramid_height - x; k++)
//			cout << ' ';
//		cout << endl;
//	}
//}
