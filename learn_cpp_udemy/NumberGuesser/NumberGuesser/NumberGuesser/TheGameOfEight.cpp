#include <iostream>

using namespace std;

void GameLoop();
bool PlayAgain();
int SwapPlayer(int);
int GetChosenNum(int, int, int);
void DetermineWinner(int, int);
void PromptCurrentPlayerToPickNumber(int, int, int);


int main()
{
	do
	{
		GameLoop();
	} while (PlayAgain());

	return 0;
}


void GameLoop()
{
	int initNum = 8;
	int chosenNum = 0;
	int currentPlayer = 1; // will always be set to 1 or 2 later

	cout << "The Game of Eight" << endl;
	cout << "=================" << endl;
	cout << endl;

	// Initial turn by player 1
	do
	{
		cout << "Player 1, pick a number. Valid choices are 1, 2, 3: ";
		cin >> chosenNum;
	} while (chosenNum < 1 || chosenNum > 3);
	initNum -= chosenNum;

	// Keep alternating turns until zero is reached
	while (initNum > 0)
	{
		currentPlayer = SwapPlayer(currentPlayer);
		chosenNum = GetChosenNum(initNum, chosenNum, currentPlayer);
		initNum -= chosenNum;
	};

	DetermineWinner(initNum, currentPlayer);
}


bool PlayAgain()
{
	char playAgain;
	const int IGNORE_CHARS = 256;
	bool failure = false;

	do
	{
		cout << "Do you want to play again (y/n)? ";
		cin >> playAgain;

		if (cin.fail())
		{
			cin.clear();
			cin.ignore(IGNORE_CHARS, '\n');
			cout << "Input error";
			failure = true;
		}
	} while (failure);

	playAgain = tolower(playAgain);

	return playAgain == 'y';
}


int SwapPlayer(int currentPlayer)
{
	if (currentPlayer == 1)
		return 2;
	else
		return 1;
}


int GetChosenNum(int initNum, int chosenNum, int currentPlayer)
{
	int newNum = 0;

	do
	{
		PromptCurrentPlayerToPickNumber(initNum, chosenNum, currentPlayer);
		cin >> newNum;
	} while (newNum < 1 || newNum > 3 || newNum == chosenNum);

	return newNum;
}


void DetermineWinner(int initNum, int currentPlayer)
{
	cout << endl << "End of game. Final number is: " << initNum << endl;
	if (currentPlayer == 1)
		if (initNum == 0)
			cout << "Player 1 wins!!!" << endl << endl;
		else
			cout << "Player 2 wins!!!" << endl << endl;
	else
		if (initNum == 0)
			cout << "Player 2 wins!!!" << endl << endl;
		else
			cout << "Player 1 wins!!!" << endl << endl;
	
	return;
}


void PromptCurrentPlayerToPickNumber(int initNum, int chosenNum, int currentPlayer)
{
	cout << endl;
	cout << "The current number is " << initNum << endl;
	cout << "Player " << currentPlayer << ", pick a number. Valid choices are ";
	if (chosenNum == 1)
		cout << "2, 3: ";
	else if (chosenNum == 2)
		cout << "1, 3: ";
	else
		cout << "1, 2: ";

	return;
}