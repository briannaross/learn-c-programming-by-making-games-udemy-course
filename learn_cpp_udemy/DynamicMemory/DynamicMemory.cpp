/*
* DynamicMemory.cpp
*
*  Created on: 18 Aug. 2017
*      Author: tashy
*/


int* CreateDynamicArray(int capacity, int &size);
void DeleteDynamicArray(int* dynamicArray, int &size);
void InsertElement(int** dynamicArray, int element, int &size, int &capacity);
void DeleteElement(int** dynamicArray, int elementIndex, int &size);
void ResizeDynamicArray(int** dynamicArray, int newCapacity);
void PrintArray(int *ptrArray, int capacity);

#include <iostream>

int main()
{
	int size = 0;
	int capacity = 0;

	int* ptrArray = CreateDynamicArray(capacity, size);

	InsertElement(&ptrArray, 10, size, capacity);
	InsertElement(&ptrArray, 20, size, capacity);
	InsertElement(&ptrArray, 30, size, capacity);
	InsertElement(&ptrArray, 40, size, capacity);
	InsertElement(&ptrArray, 50, size, capacity);
	PrintArray(ptrArray, size);

	DeleteElement(&ptrArray, 1, size);
	PrintArray(ptrArray, size);
	DeleteElement(&ptrArray, 2, size);
	PrintArray(ptrArray, size);

	DeleteDynamicArray(ptrArray, size);

	ptrArray = nullptr;

	return 0;
}


int* CreateDynamicArray(int capacity, int &size)
{
	size = 0;
	return new int[capacity];
}


void DeleteDynamicArray(int* dynamicArray, int &size)
{
	if (dynamicArray != nullptr) {
		delete[] dynamicArray;
		dynamicArray = nullptr;
	}
	size = 0;
}


void InsertElement(int** dynamicArray, int element, int &size, int &capacity)
{
	if (size > capacity)
		capacity += 2;
	else
		capacity += 1;
	ResizeDynamicArray(dynamicArray, capacity);
	(*dynamicArray)[size] = element;
	size++;
}


void DeleteElement(int** dynamicArray, int elementIndex, int &size)
{
	for (int i = elementIndex + 1; i < size; i++)
		(*dynamicArray)[i - 1] = (*dynamicArray)[i];
	size--;
}


void ResizeDynamicArray(int** dynamicArray, int newCapacity)
{
	int *newArray = new int[newCapacity];
	for (int i = 0; i < newCapacity; i++) {
		newArray[i] = (*dynamicArray)[i];
	}
	if (*dynamicArray != nullptr)
		delete[] * dynamicArray;
	*dynamicArray = newArray;
}


void PrintArray(int *dynamicArray, int size)
{
	for (int i = 0; i < size; i++) {
		std::cout << dynamicArray[i] << " ";
	}
	std::cout << std::endl;
}
