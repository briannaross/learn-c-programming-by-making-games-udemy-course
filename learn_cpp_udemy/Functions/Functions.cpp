/*
 * Functions.cpp
 *
 *  Created on: 14 Aug. 2017
 *      Author: tashy
 */

#include <iostream>

using namespace std;


void PascalsNumbers(int depth);
long double Factorial(long double n);
bool CheckIfPrime(int num);

int main()
{
	int num;

	cout << "Enter a number: ";
	cin >> num;

	PascalsNumbers(num);

// Problem 2
//	int low, high;
//
//	cout << "Enter two numbers: ";
//	cin >> low >> high;
//
//	cout << "Prime numbers between " << low << " and " << high << ": ";
//
//	for (int i = low; i <= high; i++) {
//		if (CheckIfPrime(i))
//			cout << i << " ";
//	}


//  Problem 1
//	int num;
//
//	cout << "Enter a number: ";
//	cin >> num;
//
//	if (CheckIfPrime(num))
//		cout << "Number is prime" << endl;
//	else
//		cout << "Number is not prime" << endl;

	return 0;
}


// Problem 4
void PascalsNumbers(int depth)
{
	for (int n = 0; n < depth; n++) {
		for (int i = n; i < depth; i++)
			cout << ' ';

		for (int k = 0; k <= n; k++) {
			if (n == k) {
				cout << 1 << endl;
			}
			else if (k == 0) {
				cout << 1 << " ";
			}
			else {
				cout << (Factorial(n) / (Factorial(k) * Factorial(n - k))) << " ";
			}
		}
	}

	return;
}

// Problem 4
long double Factorial(long double n)
{
	if (n == 0)	return 0;
	if (n == 1)	return 1;

	return n * Factorial (n - 1);
}


// Problems 2 & 3
bool CheckIfPrime(int num)
{
	int counter = 2;
	while (counter <= (num / counter)) {
		if ((num % counter) == 0)
			return false;

		counter++;
	}

	return true;
}
