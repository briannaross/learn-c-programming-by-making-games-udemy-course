/*
 * FileIOPractice.cpp
 *
 *  Created on: 21 Sep. 2017
 *      Author: tashy
 */
#include <iostream>
#include <fstream>
#include <string.h>
#include "Utils.h"

using namespace std;

const int MAX_NAME_SIZE = 256;
const char* INPUT_ERROR_STRING = "Input error! Please try again.";
const char* STUDENTS_FILE_NAME = "Students.bin";


struct Student
{
	int studentId;
	char firstName[256];
	char lastName[256];
	int age;
	float gpa;
};

struct StudentsDB
{
	Student* optrStudents;
	int numStudents;
	int capacity;
};

void CreateStudentsFile();
bool LoadStudents(StudentsDB& studentsDB);
void SaveStudents(const StudentsDB& studentsDB);
char GetOptionFromUser();
void ExecuteOption(char option, StudentsDB& studentsDB);
void AddOption(StudentsDB& studentsDB);
void DisplayOption(const StudentsDB& studentsDB);
void ResizeStudentsDB(StudentsDB& studentsDB, int newCapacity);

int main()
{
	char option;

	StudentsDB studentsDB;
	studentsDB.optrStudents = nullptr;
	studentsDB.numStudents = 0;
	studentsDB.capacity = 0;

	CreateStudentsFile();

	if (!LoadStudents(studentsDB))
	{
		std::cout << "Error reading file: " << STUDENTS_FILE_NAME << std::endl;
		return 0;
	}

	do {
		option = GetOptionFromUser();

		ExecuteOption(option, studentsDB);
	} while (option != 'q');

	SaveStudents(studentsDB);

	if (studentsDB.optrStudents != nullptr)
	{
		delete [] studentsDB.optrStudents;
		studentsDB.optrStudents = nullptr;
	}

	return 0;
}

void CreateStudentsFile()
{
	std::ifstream inFile;

	inFile.open(STUDENTS_FILE_NAME, std::fstream::binary);

	if (!inFile.is_open())
	{
		std::ofstream outFile;
		outFile.open(STUDENTS_FILE_NAME, std::fstream::binary);
		outFile << std::flush; // create a zero byte file
		outFile.close();
	}
	else
	{
		inFile.close();
	}
}

bool LoadStudents(StudentsDB& studentsDB)
{
	std::ifstream inFile;

	inFile.open(STUDENTS_FILE_NAME, std::fstream::binary);

	if (inFile.is_open())
	{
		inFile.seekg(0, inFile.end);
		int lengthOfFileInBytes = inFile.tellg();
		inFile.seekg(0, inFile.beg);

		int numStudents = lengthOfFileInBytes / sizeof(Student);
		int capacity = numStudents * 2 + 10;

		studentsDB.optrStudents = new Student[capacity];
		studentsDB.numStudents = numStudents;
		studentsDB.capacity = capacity;

		inFile.read((char*)studentsDB.optrStudents, lengthOfFileInBytes);

		inFile.close();

		return true;
	}

	return false;
}

void SaveStudents(const StudentsDB& studentsDB)
{
	std::ofstream outFile;

	outFile.open(STUDENTS_FILE_NAME, std::fstream::binary);

	if(outFile.is_open())
	{
		outFile.write((char*)studentsDB.optrStudents, studentsDB.numStudents * sizeof(Student));
		outFile.close();
	}
}

char GetOptionFromUser()
{
	const char options[] = {'d', 'a', 'q'};

	return GetCharacter("Please choose an option - (D)isplay Contacts, (A)dd Contacts, (Q)uit: ", INPUT_ERROR_STRING, options, 3, CC_LOWER_CASE);
}

void ExecuteOption(char option, StudentsDB& studentsDB)
{
	switch(option)
	{
	case 'a':
		AddOption(studentsDB);
		break;
	case 'd':
		DisplayOption(studentsDB);
		break;
	}
}


void AddOption(StudentsDB& studentsDB)
{
	if (studentsDB.numStudents == studentsDB.capacity)
	{
		ResizeStudentsDB(studentsDB, studentsDB.capacity * 2 + 10);
	}

	std::cout << "Enter student ID: ";
	std::cin >> studentsDB.optrStudents[studentsDB.numStudents].studentId;

	GetString("Enter first name: ", INPUT_ERROR_STRING, studentsDB.optrStudents[studentsDB.numStudents].firstName, MAX_NAME_SIZE - 1);
	GetString("Enter last name: ", INPUT_ERROR_STRING, studentsDB.optrStudents[studentsDB.numStudents].lastName, MAX_NAME_SIZE - 1);

	std::cout << "Enter age: ";
	std::cin >> studentsDB.optrStudents[studentsDB.numStudents].age;

	std::cout << "Enter gpa: ";
	std::cin >> studentsDB.optrStudents[studentsDB.numStudents].gpa;

	studentsDB.numStudents++;

	SaveStudents(studentsDB);

	std::cout << "Saved!" << std::endl;
}

void DisplayOption(const StudentsDB& studentsDB)
{
	for(int i = 0; i < studentsDB.numStudents; i++)
	{
		std::cout << "Student ID: " << studentsDB.optrStudents[i].studentId << std::endl;
		std::cout << "Name: " << studentsDB.optrStudents[i].firstName << " " << studentsDB.optrStudents[i].lastName << std::endl;
		std::cout << "Age: " << studentsDB.optrStudents[i].age << std::endl;
		std::cout << "GPA: " << studentsDB.optrStudents[i].gpa << std::endl;
		std::cout << std::endl;
	}
}

void ResizeStudentsDB(StudentsDB& studentsDB, int newCapacity)
{
	Student* newStudentsPtr = new Student[newCapacity];

	for(int i = 0; i < studentsDB.numStudents; i++)
	{
		newStudentsPtr[i].studentId = studentsDB.optrStudents[i].studentId;
		strcpy(newStudentsPtr[i].firstName, studentsDB.optrStudents[i].firstName);
		strcpy(newStudentsPtr[i].lastName, studentsDB.optrStudents[i].lastName);
		newStudentsPtr[i].age = studentsDB.optrStudents[i].age;
		newStudentsPtr[i].gpa = studentsDB.optrStudents[i].gpa;
	}

	if(studentsDB.optrStudents != nullptr)
	{
		delete [] studentsDB.optrStudents;
		studentsDB.optrStudents = nullptr;
	}

	studentsDB.optrStudents = newStudentsPtr;
	studentsDB.capacity = newCapacity;
}
