/*
 * Conditions.cpp
 *
 *  Created on: 12 Aug. 2017
 *      Author: tashy
 */

#include <iostream>

using namespace std;

int main()
{
	char c;

	cout << "Enter a character: " << endl;
	cin >> c;

	if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
		switch (c) {
			case 'A' : case 'a' : case 'E' : case 'e' : case 'I' : case 'i': case 'O' : case 'o' : case 'U' : case 'u':
				cout << c << " is a vowel" << endl;
				break;
			default:
				cout << c << " is a consonant" << endl;
				break;
		}
	}
	else {
		cout << c << " is not a letter of the alphabet" << endl;
	}


	return 0;
}


//void Problem1()
//{
//	char c;
//
//	cout << "Enter a character: " << endl;
//	cin >> c;
//
//	if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
//	{
//		cout << c << " is a letter of the alphabet" << endl;
//	}
//	else
//	{
//		cout << c << " is not a letter of the alphabet" << endl;
//	}
//}


//void Problem2()
//{
//	int i, j, k;
//
//	cout << "Enter three numbers: " << endl;
//	cin >> i >> j >> k;
//
//	if ((i > j) && (i > k))
//		cout << i;
//	else if (j > k)
//		cout << j;
//	else
//		cout << k;
//	cout << " is the biggest number, ";
//
//	if ((i < j) && (i < k))
//		cout << i;
//	else if (j < k)
//		cout << j;
//	else
//		cout << k;
//	cout << " is the smallest number." << endl;
//}


//void Problem3() {
//	float i, j, k;
//	float longest, shortA, shortB;
//
//	cout << "Triangle check" << endl;
//	cout << "Enter the lengths of three sides of a triangle: " << endl;
//	cin >> i >> j >> k;
//
//	if ((i > j) && (i > k)) {
//		longest = i;
//		shortA = j;
//		shortB = k;
//	}
//	else if (j > k) {
//		longest = j;
//		shortA = i;
//		shortB = k;
//	}
//	else { // also accounts for if all sides are equal
//		longest = k;
//		shortA = i;
//		shortB = j;
//	}
//
//	cout << "Longest side: " << longest << endl;
//	cout << "Short side A: " << shortA << endl;
//	cout << "Short side B: " << shortB << endl;
//
//	if (shortA + shortB > longest)
//		cout << "Valid triangle" << endl;
//	else
//		cout << "Invalid triangle" << endl;
//}
