/*
 * CursesUtils.cpp
 *
 *  Created on: 25 Sep. 2017
 *      Author: tashy
 */

#include "CursesUtils.h"

void InitializeCurses(bool noDelay)
{
	initscr();
	noecho();
	curs_set(false);

	nodelay(stdscr, noDelay);
	keypad(stdscr, true);
}

void ShutdownCurses()
{
	endwin();
}

void ClearScreen()
{
	clear();
}

void RefreshScreen()
{
	refresh();
}

int ScreenWidth()
{
	return COLS;
}

int ScreenHeight()
{
	return LINES;
}

int GetChar()
{
	return getch();
}

void DrawCharacter(int xPos, int yPos, char aCharacter)
{
	mvaddch(yPos, xPos, aCharacter);
}

void MoveCursor(int xPos, int yPos)
{
	move(yPos, xPos);
}

void DrawSprite(int xPos, int yPos, const char* sprite[], int spriteHeight, int colour, int offset)//, int colour)
{
	int curColour = 0;
//
	start_color();			/* Start color 			*/
	init_pair(0, COLOR_WHITE, COLOR_BLACK);
	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_MAGENTA, COLOR_BLACK);

	if (colour >= 0 && colour <= 4) {
		curColour = colour;
	}

	attron(COLOR_PAIR(curColour));
	for (int h = 0; h < spriteHeight; h++) {
		mvprintw(yPos + h, xPos, "%s", sprite[h + offset]);
	}
	attroff(COLOR_PAIR(curColour));
}

void DrawString(int xPos, int yPos, const std::string &str)
{
	mvprintw(yPos, xPos, str.c_str());
}

void DrawDebug(int xPos, int yPos, int num)
{
	mvprintw(yPos, xPos, "%d", num);
}

void DrawDebug(int xPos, int yPos, float fnum)
{
	mvprintw(yPos, xPos, "%f", fnum);
}

void DrawDebug(int xPos, int yPos, const char* str[])
{
	mvprintw(yPos, xPos, "%s", str);
}
