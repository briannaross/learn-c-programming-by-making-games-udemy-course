/*
 * CursesUtils.h
 *
 *  Created on: 25 Sep. 2017
 *      Author: tashy
 */

#ifndef CURSESUTILS_H_
#define CURSESUTILS_H_

#include <string>
#include "curses.h"

enum ArrowKeys
{
	AK_UP = KEY_UP,
	AK_DOWN = KEY_DOWN,
	AK_LEFT = KEY_LEFT,
	AK_RIGHT = KEY_RIGHT
};

void InitializeCurses(bool noDelay);
void ShutdownCurses();
void ClearScreen();
void RefreshScreen();
int ScreenWidth();
int ScreenHeight();
int GetChar();
void DrawCharacter(int xPos, int yPos, char aCharacter);
void MoveCursor(int xPos, int yPos);
void DrawSprite(int xPos, int yPos, const char* sprite[], int spriteHeight, int colour, int offset = 0);//, int colour = 0);
void DrawString(int xPos, int yPos, const std::string &str);
void DrawDebug(int xPos, int yPos, int num);
void DrawDebug(int xPos, int yPos, float fnum);
void DrawDebug(int xPos, int yPos, const char* str[]);

#endif /* CURSESUTILS_H_ */
