/*
 * StringsPractice.cpp
 *
 *  Created on: 21 Sep. 2017
 *      Author: tashy
 */

#include <iostream>
#include <string>

std::string PigLatinify(const std::string& str);

int main()
{
	std::string myString;

	std::cout << "Enter a string: ";
	std::cin >> myString;

	std::cout << "Pig latinified string: " << PigLatinify(myString) << std::endl;

	return 0;
}

std::string PigLatinify(const std::string& str)
{
	std::string plStr;

	for (unsigned int i = 1; i < str.length(); i++)
		plStr += str[i];

	plStr += str[0];
	plStr.append("ay");

	return plStr;
}
