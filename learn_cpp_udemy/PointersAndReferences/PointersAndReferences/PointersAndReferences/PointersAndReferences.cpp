#include <iostream>

using namespace std;

void Swap(int *a, int *b);

int main()
{
	cout << "Hello, world!" << endl;

	int a;
	int b;

	int *aPtr = &a;
	int *bPtr = &b;

	*aPtr = 5;
	*bPtr = 7;

	cout << *aPtr << ", " << *bPtr << endl;

	Swap(aPtr, bPtr);

	cout << *aPtr << ", " << *bPtr << endl;


	return 0;
}


void Swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}