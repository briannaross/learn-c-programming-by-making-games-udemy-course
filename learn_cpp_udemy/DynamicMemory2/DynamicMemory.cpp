/*
* DynamicMemory.cpp
*
*  Created on: 18 Aug. 2017
*      Author: tashy
*/


struct DynamicArray{
	int* dynamicArray;
	int capacity;
	int size;
};

void CreateDynamicArray(DynamicArray *da);
void DeleteDynamicArray(DynamicArray *da);
void InsertElement(DynamicArray *da, int element);
void DeleteElement(DynamicArray *da, int elementIndex);
void ResizeDynamicArray(DynamicArray *da);
void PrintArray(DynamicArray *da);

#include <iostream>

int main()
{
	DynamicArray da;
	CreateDynamicArray(&da);

	InsertElement(&da, 10);
	PrintArray(&da);
	InsertElement(&da, 20);
	PrintArray(&da);
	InsertElement(&da, 30);
	PrintArray(&da);
	InsertElement(&da, 40);
	PrintArray(&da);
	InsertElement(&da, 50);
	PrintArray(&da);

	DeleteElement(&da, 1);
	PrintArray(&da);
	DeleteElement(&da, 2);
	PrintArray(&da);
	InsertElement(&da, 60);
	PrintArray(&da);
	InsertElement(&da, 61);
	PrintArray(&da);
	InsertElement(&da, 62);
	PrintArray(&da);
	DeleteElement(&da, 0);
	PrintArray(&da);

	DeleteDynamicArray(&da);

	CreateDynamicArray(&da);

	InsertElement(&da, 70);
	PrintArray(&da);

	DeleteDynamicArray(&da);

	return 0;
}


void CreateDynamicArray(DynamicArray *da)
{
	da->size = 0;
	da->capacity = 0;
	da->dynamicArray = new int[da->capacity];
}


void DeleteDynamicArray(DynamicArray* da)
{
	if (da->dynamicArray != nullptr) {
		delete[] da->dynamicArray;
		da->dynamicArray = nullptr;
	}
	da->capacity = 0;
	da->size = 0;
}


void InsertElement(DynamicArray* da, int element)
{
	if (da->size >= da->capacity)
		da->capacity += 1;
	ResizeDynamicArray(da);
	da->dynamicArray[da->size] = element;
	da->size++;
}


void DeleteElement(DynamicArray* da, int elementIndex)
{
	for (int i = elementIndex + 1; i < da->size; i++)
		da->dynamicArray[i - 1] = da->dynamicArray[i];
	da->size--;
}


void ResizeDynamicArray(DynamicArray* da)
{
	int *newArray = new int[da->capacity];
	for (int i = 0; i < da->capacity; i++) {
		newArray[i] = da->dynamicArray[i];
	}
	if (da->dynamicArray != nullptr)
		delete[] da->dynamicArray;
	da->dynamicArray = newArray;
}


void PrintArray(DynamicArray *da)
{
	std::cout << "Size: " << da->size << ", Capacity: " << da->capacity << ", Data: ";
	for (int i = 0; i < da->size; i++) {
		std::cout << da->dynamicArray[i] << " ";
	}
	std::cout << std::endl;
}
