/*
 * Libraries.cpp
 *
 *  Created on: 21 Sep. 2017
 *      Author: tashy
 */

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <cstdlib>
#include <algorithm>


void SortVec();
void IntVecEx();
void StrVecEx();
void PrintFirstIntVecElem(const std::vector<int>&);
bool DescCompare(int x1, int x2);

int main()
{
	//IntVecEx();
	//StrVecEx();

	SortVec();

	return 0;
}

// Create a vector of MAX integers between 0 and RANDOM_MAX
void SortVec()
{
	srand(time(NULL));

	const int MAX = 20;
	const int RANDOM_MAX = 10000;

	std::vector<int> intVec;

	// Populate vector
	std::cout << "Unsorted vector" << std::endl;
	std::cout << "===============" << std::endl;
	for (int i = 0; i < MAX; i++) {
		int x = rand() % RANDOM_MAX;
		intVec.push_back(x);
		std::cout << intVec[i] << std::endl;
	}
	std::cout << std::endl;

	// Ascending sort
	sort(intVec.begin(), intVec.end());

	std::cout << "Sorted vector (ascending)" << std::endl;
	std::cout << "=========================" << std::endl;
	for (int i = 0; i < MAX; i++) {
		std::cout << intVec[i] << std::endl;
	}
	std::cout << std::endl;

	// Descending sort
	sort(intVec.begin(), intVec.end(), DescCompare); // Note that the 3rd parameter is a function parameter

	std::cout << "Sorted vector (ascending)" << std::endl;
	std::cout << "=========================" << std::endl;
	for (int i = 0; i < MAX; i++) {
		std::cout << intVec[i] << std::endl;
	}

}

// Integer vector example
void IntVecEx()
{
	std::vector<int> intVec;

	intVec.reserve(20);
	intVec.push_back(5);

	for (int i = 0; i < 20; i++) {
		intVec.push_back(5);
		std::cout << "The size of the vector is: " << intVec.size() << std::endl;
		std::cout << "The capacity of the vector is: " << intVec.capacity() << std::endl;
	}

	intVec.pop_back();
	std::cout << "The size of the vector is: " << intVec.size() << std::endl;
	std::cout << "The capacity of the vector is: " << intVec.capacity() << std::endl;

	intVec.clear(); // remove all elements in the vector

	if (intVec.empty())
		std::cout << "The vector is empty!" << std::endl;
	else
		std::cout << "The vector is not empty." << std::endl;

	intVec[0] = 5;
	PrintFirstIntVecElem(intVec);
}

void StrVecEx()
{
	// String vector
	std::cout << std::endl << std::endl;

	std::vector<std::string> strVec;

	strVec.reserve(20);
	strVec.push_back("Test");

	int capacity = 0;
	for (int i = 0; i < 1000; i++) {
		strVec.push_back("Test");
		if (strVec.capacity() > capacity) {
			std::cout << "Capacity of the vector has grown from " << capacity << " to " << strVec.capacity() << "." << std::endl;
			capacity = strVec.capacity();
		}
	}
	std::cout << "The size of the vector is: " << strVec.size() << std::endl;
	std::cout << "The capacity of the vector is: " << strVec.capacity() << std::endl;
	for (int i = 1000; i > 0; i--) {
		strVec.pop_back();
		if ((strVec.size() <= (capacity / 2)) && (capacity >= 40)) {
			strVec.shrink_to_fit();
			std::cout << "Capacity of the vector has reduced from " << capacity << " to " << strVec.capacity() << "." << std::endl;
			capacity = strVec.capacity();
		}
	}
	std::cout << "The size of the vector is: " << strVec.size() << std::endl;
	std::cout << "The capacity of the vector is: " << strVec.capacity() << std::endl;
}

void PrintFirstIntVecElem(const std::vector<int>& vecRef) { // Best way to pass vectors is by reference
	std::cout << "vecRef[0]: is " << vecRef[0] << std::endl;
}

bool DescCompare(int x1, int x2)
{
	return x1 > x2;
}
