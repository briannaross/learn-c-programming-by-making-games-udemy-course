/*
 * CursesUtils.h
 *
 *  Created on: 25 Sep. 2017
 *      Author: tashy
 */

#ifndef CURSESUTILS_H_
#define CURSESUTILS_H_

#include "curses.h"

enum ArrowKeys
{
	UP = KEY_UP,
	DOWN = KEY_DOWN,
	LEFT = KEY_LEFT,
	RIGHT = KEY_RIGHT
};

void InitializeCurses(bool noDelay);
void ShutdownCurses();
void ClearScreen();
void RefreshScreen();
int ScreenWidth();
int ScreenHeight();
int GetChar();
void DrawCharacter(int xPos, int yPos, char aCharacter);
void MoveCursor(int xPos, int yPos);


#endif /* CURSESUTILS_H_ */
