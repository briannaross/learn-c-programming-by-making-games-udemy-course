/*
 * Arrays.cpp
 *
 *  Created on: 18 Aug. 2017
 *      Author: tashy
 */


#include <iostream>

void Problem1();
void Problem2();
void Problem3();
void Problem4();
int getInt(char promptString[]);
bool validateInteger(int num, int lowVal, int highVal);
int getIntArrayLen(int array[]);

int main()
{
	Problem4();

	return 0;
}



void Problem4()
{
	int array1[] = {10, 20, 30, 40, 50, -1}; // -1 is a sentinel
	int array1Len = getIntArrayLen(array1);
	int array2Len = array1Len - 1;
	int array2[array2Len];

	int deletePos;
	do {
		deletePos = getInt("Enter a position to delete: ");
	} while (!validateInteger(deletePos, 0, array1Len - 1));

	for (int i = 0; i < deletePos; i++)
		array2[i] = array1[i];
	for (int i = deletePos + 1; i < array1Len; i++)
		array2[i - 1] = array1[i];

	for (int i = 0; i < array2Len; i++)
		std::cout << array2[i] << " ";
	 std::cout << std::endl;

}


void Problem3()
{
	int array1[] = {10, 20, 30, 40, 50, -1}; // -1 is a sentinel
	int array1Len = getIntArrayLen(array1);
	int array2Len = array1Len + 1;
	int array2[array2Len];

	int insertNum = getInt("Enter a number to insert into array: ");
	int insertPos;
	do {
		insertPos = getInt("Enter a position to insert into: ");
	} while (!validateInteger(insertPos, 0, array1Len));


	for (int i = 0; i < insertPos; i++)
		array2[i] = array1[i];
	array2[insertPos] = insertNum;
	for (int i = insertPos; i < array1Len; i++)
		array2[i + 1] = array1[i];


	for (int i = 0; i < array2Len; i++)
		std::cout << array2[i] << " ";
	 std::cout << std::endl;

}


void Problem1()
{
	char string1[] = "Hello\0";

	int str1Len = 0;
	while (string1[str1Len] != '\0')
		str1Len++;

	char string2[str1Len + 1];

	for (int i = 0; i <= str1Len - 1; i++) {
		string2[i] = string1[str1Len - 1 - i];
	}
	string2[str1Len] = '\0';

	for (int i = 0; i < str1Len - 1 - i; i++) {
		char c = string1[str1Len - 1- i];
		string1[str1Len - 1 - i] = string1[i];
		string1[i] = c;
	}

	std::cout << "Reversed string: " << string1 << std::endl;
	std::cout << "Reversed string: " << string2 << std::endl;

	return;
}


void Problem2()
{
	char string1[] = "Hello\0";

	int str1Len = 0;
	while (string1[str1Len] != '\0')
		str1Len++;

	char string2[str1Len + 1];

	for (int i = 0; i <= str1Len; i++) {
		string2[i] = string1[i];
	}

	std::cout << "String 2: " << string2 << std::endl;

	return;
}


int getInt(char promptString[])
{
	int num;
	const int IGNORE_CHARS = 256;
	bool failure = false;

	do
	{
		std::cout << promptString;
		std::cin >> num;

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(IGNORE_CHARS, '\n');
			std::cout << "Input error";
			failure = true;
		}
	} while (failure);

	return num;
}

bool validateInteger(int num, int lowVal, int highVal)
{
	return (num >= lowVal && num <= highVal);
}

int getIntArrayLen(int array[])
{
	int len = 0;
	while (array[len] != -1)
		len++;

	return len;
}
