/*
 * Utils.cpp
 *
 *  Created on: Sep 22, 2016
 *      Author: serge
 */


#include <iostream>
#include <cctype>
#include "Utils.h"

char GetCharacter(const char * prompt, const char* error)
{

	const int IGNORE_CHARS = 256;

	char input;
	bool failure;

	do
	{
		failure = false;

		std::cout << prompt;
		std::cin >> input;

		if(std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(IGNORE_CHARS, '\n');
			std::cout << error << std::endl;
			failure = true;
		}
		else
		{
			std::cin.ignore(IGNORE_CHARS, '\n');

			if(isalpha(input))
			{
				input = tolower(input);
			}
			else
			{
				std::cout << error << std::endl;
				failure = true;
			}
		}

	}while(failure);

	return input;
}

char GetCharacter(const char* prompt, const char* error, const char validInput[], int validInputLength)
{

	const int IGNORE_CHARS = 256;

	char input;
	bool failure;

	do
	{
		failure = false;

		std::cout << prompt;
		std::cin >> input;

		if(std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(IGNORE_CHARS, '\n');
			std::cout << error << std::endl;
			failure = true;
		}
		else
		{
			std::cin.ignore(IGNORE_CHARS, '\n');

			if(isalpha(input))
			{
				input = tolower(input);
			}

			for(int i = 0; i < validInputLength; i++)
			{
				if(input == validInput[i])
				{
					return input;
				}
			}

			std::cout << error << std::endl;
			failure = true;

		}

	}while(failure);

	return input;
}

void ClearScreen()
{
	//system("cls"); //for windows only!
	system("clear");
}


void WaitForKeyPress()
{
	//system("pause"); //Windows only!

	system("read -n 1 -s -p \"Press any key to continue...\";echo");
}


