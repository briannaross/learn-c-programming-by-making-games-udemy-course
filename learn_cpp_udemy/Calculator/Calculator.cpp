/*
 * Calculator.cpp
 *
 *  Created on: 22 Aug. 2017
 *      Author: tashy
 */


#include <iostream>
#include "Utils.h"

typedef int (*ArithmeticFcn)(int, int);
ArithmeticFcn GetArithmeticFcn(char op);

int Add(int x, int y);
int Sub(int x, int y);
int Mul(int x, int y);
int Div(int x, int y);
int Mod(int x, int y);
int Pow(int x, int y);


int main()
{
	char ops[] = {'+', '-', '*', '/', '%', '^'};

	int x1 = 0;
	int x2 = 0;

	std::cout << "Please input the 2 operands with a space between: ";
	std::cin >> x1 >> x2;

	char op = GetCharacter("Please input the operation: ", "Input error! Please try Again.", ops, 6);
	//char op;
	//std::cout << "Please input the operation (+ - * /): ";
	//std::cin >> op;

	ArithmeticFcn OpFcn = GetArithmeticFcn(op);

	std::cout << x1 << " " << op << " " << x2 << " = " << OpFcn(x1, x2) << std::endl;

	return 0;
}

int Add(int x, int y)
{
    return x + y;
}

int Sub(int x, int y)
{
    return x - y;
}

int Mul(int x, int y)
{
    return x * y;
}

int Div(int x, int y)
{
    return x / y;
}

int Mod(int x, int y)
{
    return x % y;
}

int Pow(int x, int y)
{
	if (y == 0)
		return 1;

	if (y == 1)
		return x;

	int result = x;
	for (int i = 2; i <= y; i++)
		result *= x;
    return result;
}


ArithmeticFcn GetArithmeticFcn(char op)
{
	switch (op)
	{
	default: // default will be to add
	case '+': return Add;
	case '-': return Sub;
	case '*': return Mul;
	case '/': return Div;
	case '%': return Mod;
	case '^': return Pow;
	}
}
