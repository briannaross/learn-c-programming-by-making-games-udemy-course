/*
 * Utils.h
 *
 *  Created on: 20 Aug. 2017
 *      Author: tashy
 */

#ifndef UTILS_H_
#define UTILS_H_


int GetIntInput();
char GetCharInput();
void ClearScreen();



#endif /* UTILS_H_ */
