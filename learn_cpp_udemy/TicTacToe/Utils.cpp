/*
 * Utils.cpp
 *
 *  Created on: 20 Aug. 2017
 *      Author: tashy
 */

#include <iostream>
#include "Utils.h"


int GetIntInput()
{
	int i;
	const int IGNORE_CHARS = 256;
	bool failure = false;

	do
	{
		failure = false;
		//std::cout << "Enter some stuff:";
		std::cin >> i;

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(IGNORE_CHARS, '\n');
			std::cout << "Input error. Please enter input again:";
			failure = true;
		}
	} while (failure == true);

	return i;
}

char GetCharInput()
{
	char c;
	const int IGNORE_CHARS = 256;
	bool failure = false;

	do
	{
		failure = false;
		//std::cout << "Enter some stuff:";
		std::cin >> c;

		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(IGNORE_CHARS, '\n');
			std::cout << "Input error. Please enter input again:";
			failure = true;
		}
	} while (failure == true);

	return c;
}


void ClearScreen() {
	for (int i = 0; i < 3; i++)
		std::cout << std::endl;
	//for (int i = 0; i < 50; i++)
	//	std::cout << std::endl;
	//system("cls") // or some such shit, maybe do a portable way to get into a good habit, like 50 newlines
}

