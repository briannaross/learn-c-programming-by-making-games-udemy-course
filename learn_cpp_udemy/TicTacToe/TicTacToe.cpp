/*
 * TicTacToe.cpp
 *
 *  Created on: 20 Aug. 2017
 *      Author: tashy
 */

#include <iostream>
#include "Utils.h"

void GameLoop();
void PrintTitle();
void DrawGrid(char* cGrid);
int SetPlayer(int iCurrentPlayer);
int GetPlayerMove(int iCurrentPlayer, char* cGrid);
void PromptCurrentPlayerForMove(int iCurrentPlayer);
bool CheckIfValidTile(int iPlayerMove);
bool CheckIfUsedTile(int iPlayerMove, char* cGrid);
void SetTile(char* cGrid, int iChosenTile, int iCurrentPlayer);
bool CheckForWinner(char* cGrid);
bool CheckForDraw(char* cGrid);
void PrintResult(bool bGameDrawn, int iCurrentPlayer);
bool PlayAgain();

int main()
{

	do
	{
		GameLoop();
	} while (PlayAgain());

	return 0;
}

void GameLoop()
{
	PrintTitle();
	// Initialisation
	bool bGameWon = false;
	bool bGameDrawn = false;
	char* cGrid = new char[9] {'0' ,'1' ,'2' ,'3' ,'4' ,'5' ,'6' ,'7' ,'8'};
	int iChosenTile = -1;
	int iCurrentPlayer = -1;

	// Game loop
	do {
		DrawGrid(cGrid);
		iCurrentPlayer = SetPlayer(iCurrentPlayer);
		iChosenTile = GetPlayerMove(iCurrentPlayer, cGrid);
		SetTile(cGrid, iChosenTile, iCurrentPlayer);
		bGameWon = CheckForWinner(cGrid);
		if (bGameWon == false)
			bGameDrawn = CheckForDraw(cGrid);
	} while ((bGameWon == false) && (bGameDrawn == false));

	DrawGrid(cGrid);

	PrintResult(bGameDrawn, iCurrentPlayer);
}


void PrintTitle()
{
	std::cout << std::endl;
	std::cout << "+-----+-----+-----+" << std::endl;
	std::cout << "| TIC |     |     |" << std::endl;
	std::cout << "+-----+-----+-----+" << std::endl;
	std::cout << "|     | TAC |     |" << std::endl;
	std::cout << "+-----+-----+-----+" << std::endl;
	std::cout << "|     |     | TOE |" << std::endl;
	std::cout << "+-----+-----+-----+" << std::endl;
	std::cout << std::endl;
}


void DrawGrid(char *cGrid)
{
		std::cout << "" << std::endl;
		std::cout << "+-----+-----+-----+" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "|  " << cGrid[0] << "  |  " << cGrid[1] << "  |  " << cGrid[2] << "  |" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "+-----+-----+-----+" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "|  " << cGrid[3] << "  |  " << cGrid[4] << "  |  " << cGrid[5] << "  |" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "+-----+-----+-----+" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "|  " << cGrid[6] << "  |  " << cGrid[7] << "  |  " << cGrid[8] << "  |" << std::endl;
		std::cout << "|     |     |     |" << std::endl;
		std::cout << "+-----+-----+-----+" << std::endl;
		std::cout << "" << std::endl;
}

int SetPlayer(int iCurrentPlayer)
{
	if (iCurrentPlayer == 1)
		iCurrentPlayer = 2;
	else
		iCurrentPlayer = 1;

	return iCurrentPlayer;
}

int GetPlayerMove(int iCurrentPlayer, char* cGrid)
{
	int iPlayerMove = -1;
	bool bValidInput = false;

	do {
		PromptCurrentPlayerForMove(iCurrentPlayer);
		iPlayerMove = GetIntInput();
		bValidInput = CheckIfValidTile(iPlayerMove);
		if (bValidInput == true)
			bValidInput = CheckIfUsedTile(iPlayerMove, cGrid);
	} while (bValidInput == false);

	return iPlayerMove;
}

void PromptCurrentPlayerForMove(int iCurrentPlayer) {
	if (iCurrentPlayer == 1)
		std::cout <<  "Player 1, enter your move [0-8]: ";
	else
		std::cout <<  "Player 2 enter your move [0-8]: ";
}

bool CheckIfValidTile(int iPlayerMove) {
	if ((iPlayerMove < 0) || (iPlayerMove > 8)) {
		std::cout <<  "Invalid Tile. Please enter a valid tile number." << std::endl;
		return false;
	}
	else
		return true;
}

bool CheckIfUsedTile(int iPlayerMove, char* cGrid)
{
		if ((cGrid[iPlayerMove] == 'X') || (cGrid[iPlayerMove] == 'O')) {
			std::cout <<  "That tile is used. Please enter an unused tile number." << std::endl;
			return false;
		}
		else
			return true;
}

void SetTile(char *cGrid, int iChosenTile, int iCurrentPlayer)
{
	if (iCurrentPlayer == 1)
		cGrid[iChosenTile] = 'X';
	else
		cGrid[iChosenTile] = 'O';
}

bool CheckForWinner(char* cGrid)
{
	if ((cGrid[0] == cGrid[1]) && (cGrid[1] == cGrid[2])) return true;
	if ((cGrid[3] == cGrid[4]) && (cGrid[4] == cGrid[5])) return true;
	if ((cGrid[6] == cGrid[7]) && (cGrid[7] == cGrid[8])) return true;
	if ((cGrid[0] == cGrid[3]) && (cGrid[3] == cGrid[6])) return true;
	if ((cGrid[1] == cGrid[4]) && (cGrid[4] == cGrid[7])) return true;
	if ((cGrid[2] == cGrid[5]) && (cGrid[5] == cGrid[8])) return true;
	if ((cGrid[0] == cGrid[4]) && (cGrid[4] == cGrid[8])) return true;
	if ((cGrid[2] == cGrid[4]) && (cGrid[4] == cGrid[6])) return true;

	return false;
}

bool CheckForDraw(char* cGrid)
{
	for (int i = 0; i < 9; i++) {
		if ((cGrid[i] != 'X') && (cGrid[i] != 'O'))
			return false;
	}

	return true;
}

void PrintResult(bool bGameDrawn, int iCurrentPlayer)
{
	if (bGameDrawn == true)
		std::cout << "Cat Game - game was a draw." << std::endl;
	else {
		if (iCurrentPlayer == 1)
			std::cout <<  "Player 1 wins!!!" << std::endl;
		else
			std::cout <<  "Player 2 wins!!!" << std::endl;
	}
}

bool PlayAgain()
{
	std::cout << "Play again (y/n)? ";
	char cPlayAgain = GetCharInput();
	if ((cPlayAgain == 'Y') || (cPlayAgain == 'y'))
		return true;

	std::cout << std::endl;
	std::cout << "Thanks for playing!" << std::endl;
	std::cout << std::endl;

	return false;
}


